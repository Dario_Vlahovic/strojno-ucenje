import matplotlib.pyplot as plt
import numpy as np
import skimage.io

img = skimage.io.imread('tiger.png', as_gray=True)
img_copy = img.copy()
svjetlina=45
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
        if(img[y,x]<=(255-svjetlina)):
            img[y,x]+=svjetlina

plt.figure(1)
plt.imshow(img, cmap='gray', vmin=0, vmax=255)

plt.figure(2)
plt.imshow(img_copy, cmap='gray', vmin=0, vmax=255)
plt.show()