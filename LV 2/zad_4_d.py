import matplotlib.pyplot as plt
import numpy as np
import skimage.io


img = skimage.io.imread('tiger.png', as_gray=True)
h,w=img.shape
minimal=10
imgreso=np.zeros((img.shape[0]//minimal,img.shape[1]//minimal))
for y in range(imgreso.shape[0]):
    for x in range(imgreso.shape[1]):
       imgreso[y,x]=img[y*minimal,x*minimal]

plt.figure(1)
plt.imshow(imgreso, cmap='gray', vmin=0, vmax=255)
plt.show()