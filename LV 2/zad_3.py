import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(open("mtcars.csv", "rb"),usecols=(1,4),delimiter=",", skiprows=1)
data1 = np.loadtxt(open("mtcars.csv", "rb"),usecols=(6),delimiter=",", skiprows=1)
plt.xlabel("mpg")
plt.ylabel("hp")
plt.scatter(data[:,0],data[:,1],linewidths=data1)
plt.show()

