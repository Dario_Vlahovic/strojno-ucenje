import matplotlib.pyplot as plt
import numpy as np
import skimage.io


img = skimage.io.imread('tiger.png', as_gray=True)
h,w=img.shape
imgmirror=np.zeros((img.shape[0],img.shape[1]))
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
       imgmirror[y,x]=img[y,img.shape[1]-1-x]

plt.figure(1)
plt.imshow(imgmirror, cmap='gray', vmin=0, vmax=255)
plt.show()