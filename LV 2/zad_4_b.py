import matplotlib.pyplot as plt
import numpy as np
import skimage.io


img = skimage.io.imread('tiger.png', as_gray=True)
h,w=img.shape
imgrot=np.zeros((img.shape[1],img.shape[0]))
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
       imgrot[x,y]=img[img.shape[0]-1-y,x]

plt.figure(1)
plt.imshow(imgrot, cmap='gray', vmin=0, vmax=255)
plt.show()
