import numpy as np
import matplotlib.pyplot as plt
import random

x=np.array([1,2,3,4,5,6],float)
xz=np.random.randint(1,7,size=100)

x1=0
x2=0
x3=0
x4=0
x5=0
x6=0
for z in xz:
    if(z==1):
        x1+=1
    if(z==2):
        x2+=1
    if(z==3):
        x3+=1
    if(z==4):
        x4+=1
    if(z==5):
        x5+=1
    if(z==6):
        x6+=1
print(x1)
print(x2)
print(x3)
print(x4)
print(x5)
print(x6)
fig,axs=plt.subplots(1,1,figsize=(10,7),tight_layout=True)
axs.hist(xz,bins=range(1,8),edgecolor="black",align="left")
plt.show()