import matplotlib.pyplot as plt
import numpy as np
import skimage.io


img = skimage.io.imread('tiger.png', as_gray=True)
h,w=img.shape
imgq1=int(img.shape[1]/4*2)
imgq2=int(img.shape[1]/4*3)
imqrt=np.zeros(img.shape)
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
       if(x>imgq1 and x< imgq2):
         imqrt[y,x]=img[y,x]
plt.figure(1)
plt.imshow(imqrt, cmap='gray', vmin=0, vmax=255)
plt.show()