import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
mtcars = pd.read_csv('mtcars.csv')
#1
#ioio=pd.DataFrame(mtcars[(mtcars.cyl==4)|(mtcars.cyl==6)|(mtcars.cyl==8)].mpg).plot.bar()
#plt.show()
#2
#mtcars.plot.box(column="wt",by="cyl")
#plt.show()

#3
#mtcars.plot(column="mpg",by="am")
#4
fig=plt.figure()
ax=fig.add_subplot()
ax.scatter(mtcars[mtcars.am==0].qsec,mtcars[mtcars.am==0].hp,color='blue',label='Manual')
ax.scatter(mtcars[mtcars.am==1].qsec,mtcars[mtcars.am==1].hp,color='pink',label='Auto')
ax.legend(loc='upper right')

plt.show()