import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
mtcars = pd.read_csv('mtcars.csv')

#1
print(mtcars.sort_values("mpg").head(5))

#2
print(mtcars[mtcars.cyl==8].sort_values("mpg",ascending=False).head(3))

#3
print(mtcars[mtcars.cyl==6].mpg.mean())

#4
print(mtcars[(mtcars.cyl==4)&(mtcars.wt>=2.0)&(mtcars.wt<=2.2)].mpg.mean())
#5
print(len(mtcars[mtcars.am==0]))
print(len(mtcars[mtcars.am==1]))
#6
print(len(mtcars[(mtcars.am==1)&(mtcars.hp>100)]))
#7
print(mtcars.wt.mul(1/2.2))