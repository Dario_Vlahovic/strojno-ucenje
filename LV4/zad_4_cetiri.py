import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# ucitavanje ociscenih podataka
df = pd.read_csv('cars_processed.csv')
print(df.info())
# razliciti prikazi
sns.pairplot(df, hue='fuel')
sns.relplot(data=df, x='km_driven', y='selling_price', hue='fuel')
df = df.drop(['name','mileage'], axis=1)
obj_cols = df.select_dtypes(object).columns.values.tolist()
num_cols = df.select_dtypes(np.number).columns.values.tolist()
fig = plt.figure(figsize=[15,8])
for col in range(len(obj_cols)):
 plt.subplot(2,2,col+1)
 sns.countplot(x=obj_cols[col], data=df)
df.boxplot(by ='fuel', column =['selling_price'], grid = False)
df.hist(['selling_price'], grid = False)
tabcorr = df.corr()
sns.heatmap(df.corr(), annot=True, linewidths=2, cmap= 'coolwarm')

#plt.show()
#1. Koliko mjerenja (automobila) je dostupno u datasetu?
#6699
#2. Kakav je tip pojedinog stupca u dataframeu?
#dtypes: float64(3), int64(4), object(5)
#3. Koji automobil ima najveću cijenu, a koji najmanju?
#kolumn=df["selling_price"]
#max_value=kolumn.min()
#print(max_value)
#najskuplji 15.789591583986285 naruti
#chevrole tavera 10.3
#4. Koliko automobila je proizvedeno 2012. godine?
#235
#5. Koji automobil je prešao najviše kilometara, a koji najmanje?
kolumn=df["km_driven"]
max_value=kolumn.max()
print(max_value)
#577414km maruti vagon
#marutti 1km
#6. Koliko najčešće automobili imaju sjedala?

#7. Kolika je prosječna prijeđena kilometraža za automobile s dizel motorom, a koliko za automobile s benzinskim
#motorom?
