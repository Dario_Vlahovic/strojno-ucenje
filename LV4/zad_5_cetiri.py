from statistics import linear_regression
from unittest import findTestCases
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler,StandardScaler
from sklearn.model_selection import train_test_split

from sklearn.metrics import mean_absolute_error,r2_score,mean_squared_error,max_error
import sklearn.linear_model as lm
df = pd.read_csv('cars_processed.csv')
print(df.info())
df = df.drop(['name','mileage'], axis=1)
y=df['selling_price']
x=df[['km_driven','year','engine','max_power']]
X_train,X_test,y_train,y_test=train_test_split(x,y,test_size=0.2,random_state=10)
print(X_train.shape)
Scaler=MinMaxScaler()
X_train_s=Scaler.fit_transform(X_train)
X_test_s=Scaler.transform(X_test)
print(X_test_s)
print(X_train_s)
linear_model=lm.LinearRegression()
linear_model.fit(X_train_s,y_train)
y_pred_train=linear_model.predict(X_test_s)
y_pred_test=linear_model.predict(X_test_s)
print("R2 Test",r2_score(y_pred_test,y_pred_test))
print("RMSE test:",np.sqrt(mean_squared_error(y_pred_test,y_test)))
print("Max error test:",max_error(y_pred_test,y_test))
print("MAE test:",mean_absolute_error(y_pred_test,y_test))
#6. Što se događa s pogreškom na testnom skupu kada mijenjate broj ulaznih veličina?
#   