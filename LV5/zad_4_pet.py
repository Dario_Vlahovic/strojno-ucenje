#Primijenite scikit-learn kmeans metodu za kvantizaciju boje na slici. Proučite kod 5.2. iz priloga vježbe te ga primijenite
#a kvantizaciju boje na slici example_grayscale.png koja dolazi kao prilog ovoj vježbi. Mijenjajte broj klastera.
#to primjećujete? Izračunajte kolika se kompresija ove slike može postići ako se koristi 10 klastera.
#Pomoću sljedećeg koda možete učitati sliku:
#import matplotlib.image as mpimg
#imageNew = mpimg.imread('example_grayscale.png')

from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

imageNew = mpimg.imread('example_grayscale.png')
    
X = imageNew.reshape((-1, 1))

plt.figure()
plt.title('Originalna slika')
plt.imshow(imageNew,  cmap='gray')

for clusterCount in range(2, 11):
    k_means = cluster.KMeans(n_clusters=clusterCount, n_init=1)
    k_means.fit(X) 
    values = k_means.cluster_centers_.squeeze()
    labels = k_means.labels_
    imageNew_compressed = np.choose(labels, values)
    imageNew_compressed.shape = imageNew.shape    
    plt.figure()
    plt.title('%d clustera' %clusterCount)
    plt.imshow(imageNew_compressed,  cmap='gray')
    plt.show()